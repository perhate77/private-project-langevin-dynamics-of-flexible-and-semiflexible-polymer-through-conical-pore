**There are two serial codes of Langevin dynamics of flexible and semiflexible polymer through conical pore, written in C.**


   
 	 

 *	 Compile code using: 
 *	 	 gcc filename.c -lm

      



 *	 Run :
 *		 nohup ./a.out &
 			


     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020
